#!/bin/bash

#	install.sh
#	install Qcom driver on Linux System
#	written by Tom 'SpuQ' Santens
#		     on:  27/09/2016
#		reviews:	10/12/2016 - changes for first Laika installer
#             26/12/2020 - progress bar instead of noise <3
#
#	note:	- run this script as root from folder where this script is
#		- it should work; I've written worse scripts ;D

# check whether script is running as root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

echo "Installing Qcom userspace driver"

echo -ne "[#                   ]                                                  \r"

# change directory to the directory of this script - paths are relative
echo -ne "[##                  ] changing working directory to $(dirname "$0")    \r"
cd "$(dirname "$0")"

# create qcom user
echo -ne "[###                 ] creating Qcom user                               \r"
useradd -r -s /bin/false qcom

# create Qcom directory '/Qdev/' in /
echo -ne "[####                ] creating Qdev directory in root for device files \r"
mkdir /Qdev/

# create Qcom directory in /opt/ for program files
echo -ne "[#####               ] creating Qcom directory in /opt/                 \r"
mkdir /opt/Qcom/

# copy the Qcom source code to /opt/Qcom/src/
echo -ne "[#######             ] copying Qdriver source code to /opt/Qcom/src/    \r"
mkdir /opt/Qcom/src/
cp ./src/* /opt/Qcom/src/

# compile Qcom driver
echo -ne "[########            ] compiling the Qcom driver C code                 \r"
make -C ./src/ > /dev/null

# move Qdriver to /Qcom/ directory
echo -ne "[###########         ] moving driver binary to /opt/Qcom/               \r"
mv ./src/QdriverV2 /opt/Qcom/

# Create symlink to driver binary in /usr/local/bin/
echo -ne "[############        ] create symlink to binary in /usr/local/bin       \r"
ln -s /opt/Qcom/QdriverV2 /usr/local/bin/


echo -ne "[#############       ]                                                   \r"


# put uninstall script in /opt/Qcom/
echo -ne "[##############      ] moving uninstall script to /opt/Qcom/            \r"
cp uninstall.sh /opt/Qcom/

# Create Qcom@.service 
cp ./other/Qcom@.service /etc/systemd/system/
# Reload systemd services
/bin/systemctl daemon-reload

# copy '10-Qcom.rules' to /etc/udev/rules.d/
echo -ne "[###############     ] installing udev rules for Qcom devices           \r"
cp ./other/10-Qcom.rules /etc/udev/rules.d/

# restart udev service
echo -ne "[################    ] restarting udev service                          \r"
service udev restart

# change ownership of /Qdev/ and it's content to qcom user
echo -ne "[#################   ] changing ownership of /Qdev/ to Qcom user        \r"
chown -R qcom:qcom /Qdev/

# udev workaround for Raspbian: restart and trigger udev on startup
# Checking for armv7l to determine if it's a Raspberry Pi
echo -ne "[##################  ] checking for Raspberry Pi                        \r"
if [[ $(uname -a | grep armv7l &>/dev/null && echo 1 || echo 0) -ne 0 ]];
then
	echo -ne "[##################  ] system is Raspberry Pi: installing udev workaround    \r"
        sed -i -e '$i \# udev workaround for Raspberry Pi' /etc/rc.local

	sed -i -e '$i \service udev restart' /etc/rc.local
        sed -i -e '$i \udevadm trigger --action=change' /etc/rc.local
fi

echo -ne "[####################] \e[92mdone                                         "
echo -ne "\e[39m \n"
exit 0;
