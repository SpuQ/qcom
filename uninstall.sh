#!/bin/sh

#	uninstall.sh
#	uninstall Qcom driver on Linux System
#	written by Tom 'SpuQ' Santens
#		on 27/09/2016
#		review	10/12/2016 - laika

# check whether script is running as root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

echo "++ Uninstalling Qcom"

# delete qcom user
echo "+ deleting Qcom user"
userdel qcom

# delete Qcom directory '/Qcom/' and its content
echo "+ deleting /Qdev/ directory and its content"
rm -rf /Qdev/

# delete logfiles
echo "+ deleting debug and logfiles from /var/log/"
rm /var/log/Qcom.log
rm /var/log/Qcom.debug

# delete '10-Qcom.rules' from /etc/udev/rules.d/
echo "+ deleting udev rules for Qcom devices"
rm /etc/udev/rules.d/10-Qcom.rules

# delete Qcom systemd service
echo "+ deleting Qcom systemd service"
rm /etc/systemd/system/Qcom@.service

# delete /opt/Qcom/ directory
echo "+ deleting /opt/Qcom/ directory and its content"
rm -rf /opt/Qcom/

# delete symlink in /usr/local/bin/
echo "+ deleting symlink in /usr/local/bin/"
rm /usr/local/bin/QdriverV2

# restart udev service
echo "+ restarting udev service"
service udev restart

# remove udev workaround
echo "+ removing udev workaround for Raspberry Pi"
sed -e "s/# udev workaround for Raspberry Pi//g" -i /etc/rc.local
sed -e "s/service udev restart//g" -i /etc/rc.local
sed -e "s/udevadm trigger --action=change//g" -i /etc/rc.local

echo "++ Qcom is uninstalled"
exit 0;
