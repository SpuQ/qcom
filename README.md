# Qcom - Linux Userspace Driver
**Qcom connects low-level hardware to high-level software. Like this:**
![Qcom is...](img/this.jpg "hardware setup")

- Easily access low-level hardware functions (e.g. Arduino) from high-level applications (e.g. NodeRED on a Raspberry Pi)


Features:

- dynamic: plug'n'play hardware modules - no worries about which tty is assigned to which Arduino ...
- device connected/disconnected notifications
- Remote Function Calls


Note:

- tested only on a Raspberry Pi 3B+ running Raspbian Stretch (Release 9.8) and an Ubuntu 18.06 machine
- Qcom will attempt to start the driver for every ttyUSB and ttyACM device that is connected to the host.
## Install / Uninstall
### Install
To install the Qcom driver, simply run install.sh
```Bash
sudo ./install.sh
```

### Uninstall
Go to the Qcom directory, and run the uninstall script.
```Bash
cd /opt/Qcom/
sudo ./uninstall.sh
```

# How it works
1. when a `ttyUSB` or `ttyACM` device is connected, the Qcom Userspace Driver is started for that device (through a udev rule in `/etc/udev/rules/10-Qcom.rules`)
2. The Qcom driver asks for the device's `Qcom name` and `Qcom address`. It does several attemts.
3. When the device answers with its name and address, a `Unix Domain Socket` is created in `/Qdev/` with the name **\<deviceName\>_\<deviceAddress\>** (e.g. sensor_1). If not, the driver exits.
4. Applications can now communicate with the device through the socket. (see examples)
5. When the device is disconnected, the Qcom driver closes and cleans up the socket.
6. Qcom drivers write to a logfile `/var/log/Qcom.log`.

## Arduino
Check out `https://gitlab.com/SpuQ/qcom-arduino` for the Qcom library for Arduino and some examples!

# License
Because of using code published under the GPLv3, I'm forced to do so with the code of the driver. But if I'm correct, there's no need to worry for your project! This is a module that runs
stand-alone, and your application is connecting to it, so you're not actually including the driver's code within your project. Therefore I believe you have the freedom to use whatever license
you like for your project. But hey, I recommend to support freedom ;)
