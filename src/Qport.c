#include "Qport.h"

/* global variables */

int cport_nr=0;
int bdrate = 0;

char mode[]={'8','N','1',0};

int _Qport_init(char* portid, int baudrate){

	if( !strcmp(portid, "ttyUSB0") ){
		cport_nr=16;
	}
	else if( !strcmp(portid, "ttyUSB1") ){
		cport_nr=17;
	}
	else if( !strcmp(portid, "ttyUSB2") ){
		cport_nr=18;
	}
	else if( !strcmp(portid, "ttyUSB3") ){
		cport_nr=19;
	}
	else if( !strcmp(portid, "ttyUSB4") ){
		cport_nr=20;
	}
	else if( !strcmp(portid, "ttyUSB5") ){
		cport_nr=21;
	}
	else if( !strcmp(portid, "ttyACM0") ){
		cport_nr=24;
	}
	else if( !strcmp(portid, "ttyACM1") ){
		cport_nr=25;
	}
	else if( !strcmp(portid, "ttyACM2") ){
		cport_nr=26;
	}
	else if( !strcmp(portid, "ttyACM3") ){
		cport_nr=27;
	}
	else {
		return -1;
	}

	// Set the baudrate
	bdrate = baudrate;

	if(RS232_OpenComport(cport_nr, bdrate, mode))
	{
		//printf("Can not open comport\n");
		return -1;
  	}

	//RS232_CloseComport(cport_nr);

	return 0;
}

int _Qport_open(void){
	if(RS232_OpenComport(cport_nr, bdrate, mode))
	{
		//printf("Can not open comport\n");
		return -1;
  	}
	return 0;
}
/*
int _Qport_read(char* input){
	int n=0, i;
	char buf[100];

	n = RS232_PollComport(cport_nr, buf, 99);

	if( n < 0 ){
		return -1;
	}

	buf[n] = 0;
	
	if(n > 0) {

		for(i=0; i < n; i++) {
        		if(buf[i] < 32) {
          			buf[i] = '.';
        		}
      		}
	}

      	sprintf(input, "%s", (char *)buf);
	//printf("Qport - got from device: %s\n", buf);
	return 0;
} */

int _Qport_read(char* buffer, int bufSize){
	int n = RS232_PollComport(cport_nr, buffer, bufSize-2);
	if( n < 0 ) return -1;

/*
	int i;
	for(i=0; i < n; i++) {
        	if(buffer[i] < 32) {
          		buffer[i] = '.';
        	}
      	}
*/
	buffer[n]='\0';

	return 0;
}

int _Qport_write(char* output){
	int n=RS232_SendBuf(cport_nr, output, strlen(output));

	if( n < 0){
		//fprintf(stderr, "Qport: err: %d\n", n);
		return -1;
	}

	return 0;
}

int _Qport_close(void){
	RS232_CloseComport(cport_nr);
}
