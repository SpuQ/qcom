#ifndef __QPORT_H__
#define __QPORT_H__

/*
 *	Qport
 * 	wrapper for RS232 interface
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "rs232.h"

int _Qport_init(char* portid, int baudrate);
int _Qport_open(void);
int _Qport_read(char* buffer, int bufSize);
int _Qport_write(char* output);
int _Qport_close(void);

//char portname[20];
#endif
