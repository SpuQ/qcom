#include "Qpack.h"

int _Qpack_pack_to_string(Qpack* pack, char* str){
	if( !str ) return -1;			// if str is NULL, exit
	if( !pack) return -1;			// if pack is NULL, exit

	if( !(*pack).signal ) return -1;		// if there's no signal in the pack, error

	if( (*pack).argument[0] == '\0'){			// if there's no argument, no need to try to pack it.
		sprintf(str, "%c%s%c", STARTBYTE, (*pack).signal, STOPBYTE);
		//printf("pack-to-string NO argument\n");
	}
	else {
		sprintf(str, "%c%s%c%s%c", STARTBYTE, (*pack).signal, SEPARATOR, (*pack).argument, STOPBYTE);
		//printf("pack-to-string WITH argument\n");
	}

	return 0;
}

int _Qpack_string_to_pack(char* str, Qpack* pack){
	if( !str ) return -1;			// if str is NULL, exit
	if( !pack) return -1;			// if pack is NULL, exit

	char* separator = strchr(str, SEPARATOR);	// search string for separator-byte
	char* stopbyte = strchr(str, STOPBYTE);
	//printf("str: %s - ", str);
	if(!stopbyte) return -1;
	*stopbyte = '\0';

	if(separator){					// if there's no separator, we assume no argument
		strcpy((*pack).argument, (char*)(separator+1) );
		*separator = '\0';
	}
	else{
		strcpy((*pack).argument, "\0");
	}
	strcpy((*pack).signal, (char*)(str+1) );

	//printf("Qpack: %s %s\n",(*pack).signal, (*pack).argument);

	return 0;
}

int _Qpack_pack_to_jsonstr(Qpack* pack, char* jsonstr){
	if( !jsonstr ) return -1;		// if jsonstr is NULL, exit
	if( !pack) return -1;			// if pack is NULL, exit

	// TODO check string size
	if( !(*pack).signal ) return -1;		// if there's no signal in the pack, error

	if( (*pack).argument ){
		// signal with argument
		sprintf(jsonstr, "{\"signal\":\"%s\",\"argument\":\"%s\"}\n", (*pack).signal, (*pack).argument);
	}
	else{
		// only a signal
		sprintf(jsonstr, "{\"signal\":\"%s\"}\n", (*pack).signal);
	}

	// notice the newline character - that made it easier to separate different packets (nodejs readline)

	return 0;
}

// TODO cleanup... dirrrrrrrtyyyyyyy !!!
// note 26/12/2020 - function causes segmentation fault. fallback to non-json write from IPC...
int _Qpack_jsonstr_to_pack(char* jsonstr, Qpack* pack){
	if( !jsonstr ) return -1;		// if jsonstr is NULL, exit
	if( !pack) return -1;			// if pack is NULL, exit

	int i;
	char *field[2];
	char *next;
	//TODO check string for object start and stop byte
	*((*pack).signal) = '\0';
	*((*pack).argument) = '\0';
	next = jsonstr;

	for(i=0;i<2;i++){
		char* this = next;
		char* end;

		if( next=strchr(next, ',') ){		// search for ',' (there after starts the next string)
			*(next)='\0';
			next = &next[1];
		}

		// get field out
		field[1] = (strchr(this,':'));	// find first separator, after this we'll find the field value later
		field[0] = (strchr(this, '"') +1);	// find beginning of field name; right after first '"' character
		end = strchr( field[0], '"');	// find end of field name;
		*end = '\0';

		// get value out
		field[1] = ( strchr(field[1], '"') +1);
		end = strchr( field[1], '"');	// find end of value
		*end = '\0';

		if( !strcmp(field[0], "signal") ){
			strcpy( (*pack).signal, field[0] );
		}
		printf("got here 1 %s\n", field[1]);
		if( !strcmp(field[1], "argument") ){
			printf("got here 2\n");
			strcpy( (*pack).argument, field[1] );
			printf("got here 3\n");
		}
	}
	printf("got here 4\n");
	if( !(*pack).signal ) return -1;	// no signal = invalid Qpack!

	return 0;
}

int _Qpack_get_from_buffer_json(char* buffer, Qpack* pack){
	//char str[1024];
	if( !buffer ) return -1;		// if buffer is NULL, exit
	if( !pack) return -1;			// if pack is NULL, exit

	if( !strrchr(buffer, '\0') ){		// search for string end (reversed), if none, exit
		return -1;
	}

	char* startbyte = (char*) 0;
	char* stopbyte = (char*) 0;

	startbyte = strchr(buffer, '{');	// find first start-byte in buffer
	stopbyte = strchr(buffer, '}');		// find first stop-byte in buffer

	// errors (changed 12/12/2016 - was this the bug?)
	// if there's no startbyte, or no stopbyte, or the stopbyte comes before the startbyte, don't continue.
	if( !stopbyte || !startbyte || (stopbyte < startbyte) ){
		// if there's a stopbyte, but no start byte, clean buffer. It's not possible that the buffer
		// would contain valid information.
		if( stopbyte && !startbyte ){
			strcpy(buffer, "\0");
		}

		// if there's no start- or stopbyte in buffer, clean buffer. (The buffer should not contain
		// data without at least having a start-byte).
		if( !stopbyte && !startbyte ){
			strcpy(buffer, "\0");
		}

		// if there's a start-byte and a stop-byte, but te stop-byte is before the start byte, throw
		// everything away before the first start byte.
		if( startbyte && stopbyte && stopbyte < startbyte ){
			strcpy(buffer, (char*)(startbyte));
		}

		return -1;
	}

	*stopbyte='\0';
	int packetLength = strlen(startbyte);
	char str[packetLength+2];				// allocate memory to fit packet
	strncpy(str, (char*)(startbyte), packetLength+1 );	// put new found package in another buffer
	strcat(str, "}\0");					// kinda stupid, but yeah...

	*stopbyte = '-';
	strcpy(buffer, stopbyte); 				// remove package from buffer

	if( _Qpack_jsonstr_to_pack(str, pack) < 0 ){		// convert JSON pack-string to Qpack format
		return -1;
	}

	return 0;
}

int _Qpack_get_from_buffer(char* buffer, Qpack* pack){
	//char str[SIGNALSIZE+ARGUMENTSIZE+10];

	if( !buffer ) return -1;			// if buffer is NULL, exit
	if( !pack) return -1;				// if pack is NULL, exit

	if( !strrchr(buffer, '\0') ){			// search for string end (reversed), if none, exit
		return -1;
	}

	char* startbyte = strchr(buffer, STARTBYTE);	// find first start-byte in buffer
	char* stopbyte = strchr(buffer, STOPBYTE);	// find first stop-byte in buffer

	// errors (changed 12/12/2016 - was this the bug?)
	// if there's no startbyte, or no stopbyte, or the stopbyte comes before the startbyte, don't continue.
	if( !stopbyte || !startbyte || (stopbyte < startbyte) ){
		// if there's a stopbyte, but no start byte, clean buffer. It's not possible that the buffer
		// would contain valid information.
		if( stopbyte && !startbyte ){
			strcpy(buffer, "\0");
		}

		// if there's no start- or stopbyte in buffer, clean buffer. (The buffer should not contain
		// data without at least having a start-byte).
		if( !stopbyte && !startbyte ){
			strcpy(buffer, "\0");
		}

		// if there's a start-byte and a stop-byte, but te stop-byte is before the start byte, throw
		// everything away before the first start byte.
		if( startbyte && stopbyte && stopbyte < startbyte ){
			strcpy(buffer, (char*)(startbyte));
		}

		return -1;
	}

	*stopbyte='\0';
	int packetLength = strlen(startbyte);
	char str[packetLength+2];				// allocate memory to fit packet

	strncpy(str, (char*)(startbyte), ( packetLength+1) ); 	// put new found package in another buffer
	strcat(str, "}\0");					// kinda stupid, but yeah...

	*stopbyte= '-';
	strcpy(buffer, stopbyte); 				// remove package from buffer

	if( _Qpack_string_to_pack(str, pack) < 0 ){		// convert pack-string to Qpack format
		return -1;
	}

	return 0;
}

void _Qpack_print_pack(Qpack* pack){
	fprintf(stderr, "Qpack signal: %s, argument: %s\n", (*pack).signal, (*pack).argument);
}
